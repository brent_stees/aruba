module.exports = function(grunt) {

    grunt.initConfig({

        // Package
        pkg: grunt.file.readJSON('package.json'),

        // Compass
        compass: {
            build: {
                options: {
                    sassDir: 'assets/sass',
                    cssDir: 'assets/css',
                    outputStyle: 'compressed',
                    // trace: true,
                    // debugInfo: true
                }
            }
        },

        // Clean
        clean: {
            pre: ['styleguide', 'assets/css'],
            post: ['.sass-cache']
        },

        // Watch
        watch: {
            sass: {
                files: ['assets/sass/**/*.{sass,scss}'],
                tasks: ['compass', 'sassdown']
            }
        },

        // Sassdown (Styleguide)
        sassdown: {
            options: {
                assets: [
                    'assets/css/*.css',
                    'styleguide/css/*.css',
                    'assets/js/mq.genie.min.js'
                    ],
                handlebarsHelpers: [
                    'node_modules/handlebars-helpers/lib/helpers/helpers-collections.js',
                    'node_modules/handlebars-helpers/lib/helpers/helpers-strings.js'
                ],
                template: 'templates/template.hbs'
            },
            files: {
                expand: true,
                cwd: 'assets/sass/partials',
                src: ['**/*.{sass,scss}'],
                dest: 'styleguide/'
            }
        },

        // Debugging
        'node-inspector': {
          dev: {}
        },

        // copy images
        copy: {
            dev: {
                files: [{
                    cwd: 'assets/images',
                    src: '*',
                    dest: 'styleguide/images',
                    expand: true
                },
                {
                    cwd: 'lib',
                    src: 'styleguide.css',
                    dest: 'styleguide/css',
                    expand: true
                }]
            }
        }

    });


    // Load NPM Tasks
    grunt.loadNpmTasks('sassdown');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Register Grunt tasks
    grunt.registerTask('default', ['clean:pre', 'copy', 'compass', 'sassdown', 'clean:post']);
    grunt.registerTask('sg', ['clean:pre', 'copy', 'sassdown', 'clean:post']);
};
