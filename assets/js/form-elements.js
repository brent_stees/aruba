$(document).ready(function () {
    // Icon and class inits
    $('.has-icon').each(function () {
        $(this).append('<span class="icon"></span>');
    });
    var form_box = $('.form-box');
    form_box.each(function () {
        $(this).find('header h4').addClass('open');
    });

    // Expandable section
    form_box.find('header h4').click(function () {
        var initial_width = $(this).outerWidth();
        $(this).closest('.form-box').find('.form-container').slideToggle();
        $(this).toggleClass('open');
        $(this).css('width', ceil(initial_width));
    });
});